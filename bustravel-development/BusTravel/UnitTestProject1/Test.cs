﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data;
using RutaBussiness;
using BusTravel;

namespace UnitTestProject1
{
    [TestClass]
    public class Test
    {
        public Ruta r = new Ruta();
        public RutaRepository rr = new RutaRepository();

        [TestInitialize]

        public void init()
        {
            r.Id = 1;
            r.Grad1 = "grad1";
            r.Grad2 = "grad2";
            r.Vpolaska = "vpolaska";
            r.Vdolaska = "vdolaska";
            r.Udaljenost = "1 km";
            r.Prevoznik = "prevoznik";
        }


        [TestMethod]
        public void TestMethod1()
        {
            foreach (Ruta rt in rr.GetRute())
            {
                if (rt.Id ==r.Id)
                    r.Id = rt.Id;
            }
            if (r.Id == ' ')
                Assert.Fail();

        }
        [TestMethod]
        public void TestMethod2()
        {

            rr.UpdateRuta(r);
            foreach (Ruta rt in rr.GetRute())
            {
                if (rt.Id==r.Id)
                    r.Id = rt.Id;

            }
            if (r.Id == ' ')
                Assert.Fail("Greska");
        }
        [TestMethod]
        public void TestMethod3()
        {

            rr.DeleteRuta(r.Id);
            foreach (Ruta rt in rr.GetRute())
            {
                if (rt.Id==r.Id)
                    Assert.Fail("Greska");

            }


        }

        [TestCleanup]

        public void cleanup()
        {
            rr.DeleteRuta(r.Id);
        }

    }
}
