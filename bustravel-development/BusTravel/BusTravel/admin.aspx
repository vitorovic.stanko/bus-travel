﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="BusTravel.admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bus Travel</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/JFFormStyle-1.css" />
<link rel="apple-touch-icon" sizes="180x180" href="images/apple.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/velika.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/mala.png">
<link rel="manifest" href="images/site.webmanifest">
<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!-- //js -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->	
<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
<!--pop-up-->
<script src="js/menu_jquery.js"></script>
<!--//pop-up-->	
</head>
<body>
    <form id="form1" runat="server">
        <div>
   <!--header-->
	<div class="container glavni">
	<div class="header">
		<div class="container">
			<div class="header-grids">
				<div class="logo">
					<h1><a  href="index.aspx"><span>Bus</span>Travel</a></h1>
				</div>
				<!--navbar-header-->
				<div class="header-dropdown">
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="nav-top">
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt="" /></span>
					<ul class="nav1">
						<li ><a href="index.aspx">Početak</a></li>
						<li ><a href="onama.aspx">O nama</a></li>
						<li><a href="kontakt.aspx">Kontakt</a></li>
						<div class="dropdown-grids d-lg-block d-none ">         
							<form class="form-inline forma" action="/action_page.php">
                                <asp:Button ID="Button4" runat="server" Text="Odjavi se" class="dugme2" style="margin-top:5%;" OnClick="Button4_Click"/>
							  </form>
			</div>
					</ul>
					<div class="clearfix"> </div>
					<!-- script-for-menu -->
							 <script> 
							   $( "span.menu" ).click(function() {
								 $( "ul.nav1" ).slideToggle( 300, function() {
								 // Animation complete.
								  });
								 });
							
							</script>
						<!-- /script-for-menu -->

				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//header-->
	<!-- banner -->
	<div class="container banner ">
		<!-- container -->
		
			<div class="col-lg-4 col-md-4 banner-right">
				<div class="sap_tabs">	
					<div class="booking-info">
                        <h2>Unesi podatke o ruti</h2>
                        
                         
                                <div class="form-group">
                                  <label for="formGroupExampleInput" style="color: white;"><h4>Polazište</h4></label>
                                    <asp:TextBox ID="TextBox2" runat="server" placeholder="Grad polaska" class="form-control"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                  <label for="formGroupExampleInput2" style="color: white;"><h4>Dolazište</h4></label>
                                    <asp:TextBox ID="TextBox3" runat="server" placeholder="Grad dolaska" class="form-control"></asp:TextBox>
                                </div>
                              
                      
                       
					</div>
				
						<!---->
					</div> 	
                </div>

                <div class="col-lg-4 col-md-4 banner-right" style="padding-bottom: 3%;">
                    <div class="sap_tabs">	
                        <div class="booking-info">
                            <h2>Vreme</h2>
                            
                              
                                    <div class="form-group">
                                      <label for="formGroupExampleInput" style="color: white;"><h4>Vreme polaska</h4></label>
                                        <asp:TextBox ID="TextBox4" runat="server" placeholder="Vreme polaska" class="form-control"  ></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                      <label for="formGroupExampleInput2" style="color: white;"><h4>Vreme dolaska</h4></label>
                                        <asp:TextBox ID="TextBox5" runat="server" placeholder="Vreme dolaska" class="form-control"></asp:TextBox>
                                    </div>
                              
                          
                           
                        </div>
                    
                            <!---->
                        </div> 	
                    </div>
                    <div class="col-lg-4 col-md-4 banner-right">
                        <div class="sap_tabs">	
                            <div class="booking-info">
                                <h2>Ostalo</h2>
                                
                                  
                                        <div class="form-group">
                                          <label for="formGroupExampleInput" style="color: white;"><h4>Rastojanje</h4></label>
                                            <asp:TextBox ID="TextBox1" runat="server" placeholder="Rastojanje" class="form-control"></asp:TextBox>
                                          
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="formGroupExampleInput2" style="color: white;"><h4>Prevoznik</h4></label>                                   
                                            <asp:TextBox ID="TextBox6" runat="server" placeholder="Ime prevoznika" class="form-control"></asp:TextBox>
                                        </div>
                                      
                              
                               
                            </div>
                        
                                <!---->
                            </div> 	
                        </div>
                        
                        <div class="col-lg-12 text-center dugmici">
                            <asp:Button ID="Button3" runat="server" Text="Dodaj" class="dugme2" OnClick="Button3_Click" />
                            <asp:Button ID="Button1" runat="server" Text="Izmeni" class="dugme2" OnClick="Button1_Click" />
                            <asp:Button ID="Button2" runat="server" Text="Obriši" class="dugme2" OnClick="Button2_Click" />
                        </div>
                     <div class="col-lg-12 text-center dugmici">
                            <asp:TextBox ID="TextBox7" runat="server" placeholder="Izaberi ID" class=" mx-auto d-block" style="width:20%" ></asp:TextBox>
                             <asp:Button ID="Button5" runat="server" Text="Izaberi" class="dugme2" OnClick="Button5_Click" />
                     
                
                        </div>
                        <div class="col-lg-12 banner-right" style="padding-top: 4%;">
                            <div class="sap_tabs">	
                                <div class="booking-info">
                                    <h2 class="text-center" style="color: white;">Prikaz postojećih ruta</h2>
                                    <asp:Table class="table table-dark text-center tabela" style="background-color:white" ID="Table1" runat="server"></asp:Table>
                        </div>
                </div>
            </div>

</div>
            
			<div class="clearfix"> </div>
		
		<!-- //container -->
	
	<!-- banner-bottom -->
	<div class="">
		<!-- container -->
					<div class="clearfix"> </div>
					<div class="copyright">
						<p>Copyrights © 2019 BusTravel . Design by <a href="onama.aspx">iCreate</a></p>
					</div>
				</div>
		</div>
	</div>
	<script defer src="js/jquery.flexslider.js"></script>
	<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript">
		$(function(){
			SyntaxHighlighter.all();
			});
			$(window).load(function(){
			$('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			$('body').removeClass('loading');
			}
			});
		});
	</script>	
	</div>	
        
        </form>
</body>
</html>
