﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kontakt.aspx.cs" Inherits="BusTravel.kontakt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bus Travel</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/JFFormStyle-1.css" />
<link rel="apple-touch-icon" sizes="180x180" href="images/apple.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/velika.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/mala.png">
<link rel="manifest" href="images/site.webmanifest">
<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!-- //js -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->	
<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
<!--pop-up-->
<script src="js/menu_jquery.js"></script>
<!--//pop-up-->	
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <!--header-->
	<div class="container glavni">
	<div class="header">
		<div class="container">
			<div class="header-grids">
				<div class="logo">
					<h1><a  href="index.aspx"><span>Bus</span>Travel</a><asp:Label class="login1" ID="Label2" runat="server" Text="Neispravno korisničko ime ili šifra"></asp:Label></h1>
				</div>
				<!--navbar-header-->
				<div class="header-dropdown">
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="nav-top">
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt="" /></span>
					<ul class="nav1">
						<li><a href="index.aspx">Početak</a></li>
						<li><a href="onama.aspx">O nama</a></li>
						<li  class="active" ><a href="kontakt.aspx">Kontakt</a></li>
						<div class="dropdown-grids forma">         
							<form class="form-inline " action="/action_page.php">
								<label for="email">Korisničko ime:</label>	
                                <asp:TextBox id="email" placeholder="Unesi korisničko ime" name="email"  runat="server"></asp:TextBox>
								<label for="pwd">Šifra:</label>	
                                <asp:TextBox type="password" id="pwd" placeholder="Unesi šifru" name="pswd" runat="server"></asp:TextBox>					
                                <asp:Button class="dugme2" type="submit" ID="Button1" runat="server" Text="Prijavi se" OnClick="Button1_Click1" />

							  </form>
			</div>
					</ul>
					<div class="clearfix"> </div>
					<!-- script-for-menu -->
							 <script> 
							   $( "span.menu" ).click(function() {
								 $( "ul.nav1" ).slideToggle( 300, function() {
								 // Animation complete.
								  });
								 });
							
							</script>
						<!-- /script-for-menu -->

				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//header-->
	<!-- banner -->
	<div class="container banner">
		<!-- container -->
		
			<div class="col-lg-4 col-md-4  banner-left">
				<section class="slider2">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<div class="slider-info">
									<img src="images/1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="slider-info">
									<img src="images/2.jpg" alt="">
								</div>
							</li>
							<li>	
								<div class="slider-info">
									<img src="images/3.jpg" alt="">
								</div>
							</li>
							<li>	
								<div class="slider-info">
									<img src="images/44.jpg" alt="">
								</div>
							</li>
							<li>	
								<div class="slider-info">
									<img src="images/55.jpg" alt="">
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--FlexSlider-->
			</div>
			<div class="col-lg-8 col-md-8 banner-right">
				<div class="sap_tabs">	
					<div class="booking-info">
						<h2>Kontakt</h2>
                        <div >
                            <p style="color:white"> <strong> Naziv :</strong> ICreate d.о.о.&nbsp;Čačak <br><br> <strong>Adresa:</strong>&nbsp;Svetog Save 66<br><br> <strong>Мatični broj:</strong> 20916834<br><br> <strong>PIB:</strong> 108018053<br><br> <strong>Kontakt telefon agencije (fix):</strong>&nbsp;+381 565423<br><br><strong>Kontakt telefon agencije (mob):</strong>&nbsp;+381 642 5713<br> <br><strong>Kontakt email:</strong>&nbsp;bustravel@gmail.com</p>
                        </div>
					</div>
                    
				
						<!---->
					</div> 	
				</div>
			</div>
			<div class="clearfix"> </div>
		
		<!-- //container -->
	</div>
	<!-- banner-bottom -->
	<div class="">
		<!-- container -->
					<div class="clearfix"> </div>
					<div class="copyright">
						<p>Copyrights © 2019 BusTravel . Design by <a href="onama.aspx">iCreate</a></p>
					</div>
				</div>
		</div>
	</div>
	<script defer src="js/jquery.flexslider.js"></script>
	<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript">
		$(function(){
			SyntaxHighlighter.all();
			});
			$(window).load(function(){
			$('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			$('body').removeClass('loading');
			}
			});
		});
	</script>	
	</div>	
        </div>
    </form>
</body>
</html>

