﻿using Data;
using RutaBussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BusTravel
{
    public partial class admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Admin.curadmin == null)
            {
                Response.Redirect("Index.aspx");
            }
            RutaB r1 = new RutaB();
            List<Ruta> lista = r1.GetAllRute();
            TableCell c0 = new TableCell();
            c0.Text = "ID";
            TableCell c1 = new TableCell();
            c1.Text = "Polazište";
            TableCell c2 = new TableCell();
            c2.Text = "Dolazište";
            TableCell c3 = new TableCell();
            c3.Text = "Vreme polaska";
            TableCell c4 = new TableCell();
            c4.Text = "Vreme dolaska";
            TableCell c5 = new TableCell();
            c5.Text = "Udaljenost";
            TableCell c6 = new TableCell();
            c6.Text = "Prevoznik";
            TableRow row = new TableRow();
            row.Cells.Add(c0);
            row.Cells.Add(c1);
            row.Cells.Add(c2);
            row.Cells.Add(c3);
            row.Cells.Add(c4);
            row.Cells.Add(c5);
            row.Cells.Add(c6);
            Table1.Rows.Add(row);

            foreach (Ruta r in lista)
            {
                TableCell cID = new TableCell();
                cID.Text = r.Id.ToString();
                TableCell c7 = new TableCell();
                c7.Text = r.Grad1;
                TableCell c8 = new TableCell();
                c8.Text = r.Grad2;
                TableCell c9 = new TableCell();
                c9.Text = r.Vpolaska;
                TableCell c10 = new TableCell();
                c10.Text = r.Vdolaska;
                TableCell c11 = new TableCell();
                c11.Text = r.Udaljenost;
                TableCell c12 = new TableCell();
                c12.Text = r.Prevoznik;
                TableRow row2 = new TableRow();
                row2.Cells.Add(cID);
                row2.Cells.Add(c7);
                row2.Cells.Add(c8);
                row2.Cells.Add(c9);
                row2.Cells.Add(c10);
                row2.Cells.Add(c11);
                row2.Cells.Add(c12);
                Table1.Rows.Add(row2);

            }

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Admin.curadmin = null;
            Response.Redirect("index.aspx");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            RutaB r1 = new RutaB();
            Ruta r = new Ruta();
            List<Ruta> lista = r1.GetAllRute();
            foreach(Ruta ru in lista)
            {
                if (ru.Id == Convert.ToInt32(TextBox7.Text))
                    r = ru;
            }
            TextBox2.Text = r.Grad1;
            TextBox3.Text = r.Grad2;
            TextBox4.Text = r.Vpolaska;
            TextBox5.Text = r.Vdolaska;
            TextBox1.Text = r.Udaljenost;
            TextBox6.Text = r.Prevoznik;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            RutaB r1 = new RutaB();
            Ruta r = new Ruta();
            List<Ruta> lista = r1.GetAllRute();
            foreach (Ruta ru in lista)
            {
                if (ru.Id == Convert.ToInt32(TextBox7.Text))
                    r = ru;
            }
            r.Grad1 = TextBox2.Text;
            r.Grad2 = TextBox3.Text;
            r.Vpolaska = TextBox4.Text;
            r.Vdolaska = TextBox5.Text;
            r.Udaljenost = TextBox1.Text;
            r.Prevoznik = TextBox6.Text;
            r1.UpdateRuta(r);
            Response.Redirect("admin.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            RutaB r1 = new RutaB();
            Ruta r = new Ruta();
            List<Ruta> lista = r1.GetAllRute();
            foreach (Ruta ru in lista)
            {
                if (ru.Id == Convert.ToInt32(TextBox7.Text))
                    r = ru;
            }
            r1.DeleteRuta(r);
            Response.Redirect("admin.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            RutaB r1 = new RutaB();
            Ruta r = new Ruta();
            List<Ruta> lista = r1.GetAllRute();
            r.Grad1 = TextBox2.Text;
            r.Grad2 = TextBox3.Text;
            r.Vpolaska = TextBox4.Text;
            r.Vdolaska = TextBox5.Text;
            r.Udaljenost = TextBox1.Text;
            r.Prevoznik = TextBox6.Text;
            r1.InsertRuta(r);
            Response.Redirect("admin.aspx");
        }
    }
}