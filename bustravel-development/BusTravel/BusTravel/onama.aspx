﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="onama.aspx.cs" Inherits="BusTravel.onama" %>

<!DOCTYPE html>
<html>
<head>
<title>Bus Travel</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/JFFormStyle-1.css" />
<link rel="apple-touch-icon" sizes="180x180" href="images/apple.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/velika.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/mala.png">
<link rel="manifest" href="images/site.webmanifest">
<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!-- //js -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->	
<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
<!--pop-up-->
<script src="js/menu_jquery.js"></script>
<!--//pop-up-->	
</head>
<body>
	<!--header-->
	<div class="container glavni">
	<div class="header">
		<div class="container">
			<div class="header-grids">
				<div class="logo">
					<h1><a  href="index.aspx"><span>Bus</span>Travel</a><asp:Label class="login1" ID="Label2" runat="server" Text="Neispravno korisničko ime ili šifra"></asp:Label></h1>
				</div>
				<!--navbar-header-->
				<div class="header-dropdown">
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="nav-top">
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt="" /></span>
					<ul class="nav1">
						<li ><a href="index.aspx">Početak</a></li>
						<li class="active"><a href="onama.aspx">O nama</a></liclass="active">
						<li><a href="kontakt.aspx">Kontakt</a></li>
						<div class="dropdown-grids forma">         
							<form runat="server" class="form-inline ">
								<label for="email">Korisničko ime:</label>	
                                <asp:TextBox id="email3" placeholder="Unesi korisničko ime" name="email"  runat="server"></asp:TextBox>
								<label for="pwd">Šifra:</label>	
                                <asp:TextBox type="password" id="pwd3" placeholder="Unesi šifru" name="pswd" runat="server"></asp:TextBox>					
                                <asp:Button class="dugme2" type="submit" ID="Button1" runat="server" Text="Prijavi se" OnClick="Button1_Click1" />

							  </form>
			</div>
					</ul>
					<div class="clearfix"> </div>
					<!-- script-for-menu -->
							 <script> 
							   $( "span.menu" ).click(function() {
								 $( "ul.nav1" ).slideToggle( 300, function() {
								 // Animation complete.
								  });
								 });
							
							</script>
						<!-- /script-for-menu -->

				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//header-->
	<!-- banner -->
	<div class="container banner ">
		<!-- container -->
		
			<div class="col-lg-12 col-md-8 banner-right">
				<div class="sap_tabs sap">	
					<div class="booking-info">
                        <h2>Bus Travel</h2>
                        <h3 style="color: white;">Misija</h3><br>
                        <p style="color: white;"> Naša misija je da olakšamo planiranje putovanja u Srbiji organizovanjem informacija o autobuskom redu vožnje i tako ga učinimo lako dostupnim na mreži.</p>
                        <br>
                        <h3 style="color: white;">Vizija</h3><br>
                        <p style="color: white;">Naša vizija je da budemo korisnički orijentisani i preferirani pružalac informacija o autobuskom redu vožnje.</p>
                        <br>
                        <h3 style="color:#6fd508;">iCreate</h3><br>
                        <p style="color: white;">iCreate TIM VAM ŽELI VEDRA JUTRA, MIRNE VETROVE I SIGURNE PUTEVE!</p>
                        <br />
                         <p style="color: white;">Ranko Nikolić, Dragan Joksimović, Stanko Vitorović i Nikola Stojanović</p><br<<br><br>
					</div>
				
						<!---->
					</div> 	
                </div>
                
			</div>
			<div class="clearfix"> </div>
		
		<!-- //container -->
	</div>
	<!-- banner-bottom -->
	<div class="">
		<!-- container -->
					<div class="clearfix"> </div>
					<div class="copyright">
						<p>Copyrights © 2019 BusTravel . Design by <a href="onama.aspx">iCreate</a></p>
					</div>
				</div>
		</div>
	</div>
	<script defer src="js/jquery.flexslider.js"></script>
	<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript">
		$(function(){
			SyntaxHighlighter.all();
			});
			$(window).load(function(){
			$('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			$('body').removeClass('loading');
			}
			});
		});
	</script>	
	</div>	
</body>
</html>