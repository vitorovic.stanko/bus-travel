﻿using Data;
using RutaBussiness;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace BusTravel
{

    public partial class WebForm1 : System.Web.UI.Page
    {
        private RutaB r;
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Visible = false;
            Table1.Visible = false;
            Label2.Visible = false;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            AdminB a = new AdminB();
            if (a.ProveriAdmina(email.Text,pwd.Text))
            {
                Response.Redirect("admin.aspx");
            }
            else
            Label1.Visible = true;


        }
        protected void ruta(object sender, EventArgs e)
        {
            RutaB r1 = new RutaB();
            List<Ruta> lista = r1.GetOdredjeneRute(TextBox1.Text,TextBox2.Text);
            TableCell c1 = new TableCell();
            c1.Text = "Vreme polaska";
            TableCell c2 = new TableCell();
            c2.Text = "Vreme dolaska";
            TableCell c3 = new TableCell();
            c3.Text = "Udaljenost";
            TableCell c4 = new TableCell();
            c4.Text = "Prevoznik";
            TableRow row = new TableRow();
            row.Cells.Add(c1);
            row.Cells.Add(c2);
            row.Cells.Add(c3);
            row.Cells.Add(c4);
            Table1.Rows.Add(row);

           foreach (Ruta r in lista)
           {
                TableCell c5 = new TableCell();
                c5.Text = r.Vpolaska;
                TableCell c6 = new TableCell();
                c6.Text = r.Vdolaska;
                TableCell c7 = new TableCell();
                c7.Text = r.Udaljenost;
                TableCell c8 = new TableCell();
                c8.Text = r.Prevoznik;
                TableRow row2 = new TableRow();
                row2.Cells.Add(c5);
                row2.Cells.Add(c6);
                row2.Cells.Add(c7);
                row2.Cells.Add(c8);
                Table1.Rows.Add(row2);

            }
            if (lista.Count > 0)
                Table1.Visible = true;
            else
            {
                Label2.Text = "Ruta ne postoji";
                Label2.Visible = true;
                TextBox1.Text = "";
                TextBox2.Text = "";
            }

        }
    }
}