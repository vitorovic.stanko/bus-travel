﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutaBussiness
{
    public class RutaB
    {
        private RutaRepository r;

        public RutaB()
        {
            this.r = new RutaRepository();
        }
        public List<Ruta> GetAllRute()
        {
            return this.r.GetRute();
        }
        public List<Ruta> GetOdredjeneRute(string po,string doo)
        {

            return this.r.GetRute().Where(x => x.Grad1.Equals(po) && x.Grad2.Equals(doo)).ToList();
        }
        public string InsertRuta(Ruta r)
        {
            int rows = this.r.InsertRuta(r);
            if (rows > 0)
            {
                return "Uspesan unos u bazu!";
            }
            else
                return "Neuspesan unos!";

        }
        public string UpdateRuta(Ruta r)
        {
            int rows = this.r.UpdateRuta(r);
            if (rows > 0)
            {
                return "Uspesan unos u bazu!";
            }
            else
                return "Neuspesan unos!";

        }
        public string DeleteRuta(Ruta r)
        {
            int rows = this.r.DeleteRuta(r.Id);
            if (rows > 0)
            {
                return "Uspesan brisanje iz baze!";
            }
            else
                return "Neuspesano brisanje iz baze!";

        }
    }
}
