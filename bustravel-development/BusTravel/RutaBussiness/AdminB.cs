﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutaBussiness
{
    public class AdminB
    {
        private AdminRepository a;

        public AdminB()
        {
            this.a = new AdminRepository();
        }
        public List<Admin> GetAdmins()
        {
            return this.a.listaadmina();
        }
        public Boolean ProveriAdmina(string a,string b)
        {

            List<Admin> lista =  this.a.listaadmina().Where(x => x.Username.Equals(a) && x.Password.Equals(b)).ToList();
            if (lista.Count>0)
            {
                Admin.curadmin = lista.ElementAt(0);
                return true;
            }
            return false;
               
        }

    }
}
