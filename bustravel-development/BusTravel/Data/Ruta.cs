﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Ruta
    {
        private int id;
        private string grad1;
        private string grad2;
        private string vpolaska;
        private string vdolaska;
        private string udaljenost;
        private string prevoznik;

        public int Id { get => id; set => id = value; }
        public string Grad1 { get => grad1; set => grad1 = value; }
        public string Grad2 { get => grad2; set => grad2 = value; }
        public string Vpolaska { get => vpolaska; set => vpolaska = value; }
        public string Vdolaska { get => vdolaska; set => vdolaska = value; }
        public string Udaljenost { get => udaljenost; set => udaljenost = value; }
        public string Prevoznik { get => prevoznik; set => prevoznik = value; }
    }
}
