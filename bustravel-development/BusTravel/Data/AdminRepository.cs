﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class AdminRepository
    {
        public List<Admin> listaadmina()
        {
            List<Admin> lista = new List<Admin>();
            SqlConnection sqlKonekcija = new SqlConnection();
            {
                //sqlKonekcija.ConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=bustravelbaza;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                sqlKonekcija.ConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Bus;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                sqlKonekcija.Open();
                string query = "SELECT * FROM dbo.Bus ";
                SqlCommand sqlCmd = new SqlCommand(query, sqlKonekcija);

                SqlDataReader readit = sqlCmd.ExecuteReader();
                while (readit.Read())
                {
                    string username = readit.GetString(0);
                    string password = readit.GetString(1);
                    Admin a = new Admin(username, password);
                    lista.Add(a);

                } 
                sqlKonekcija.Close();
                return lista;

            }
        }


    }
}
