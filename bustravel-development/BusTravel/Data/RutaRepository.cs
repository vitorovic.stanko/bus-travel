﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Data
{
    public class RutaRepository
    {

        // private string cstring = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=bustravelbaza;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
       private string cstring = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Bus;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Ruta> GetRute()
        {
            using (SqlConnection konekcija = new SqlConnection(cstring))
            {
                konekcija.Open();
                SqlCommand cmd1 = new SqlCommand();
                cmd1.Connection = konekcija;
                cmd1.CommandText = "SELECT * FROM dbo.Ruta";
                SqlDataReader readit = cmd1.ExecuteReader();
                List<Ruta> lista = new List<Ruta>();
                while (readit.Read())
                {
                    Ruta r = new Ruta();
                  
                    r.Id = readit.GetInt32(0);
                    r.Grad1 = readit.GetString(1);
                    r.Grad2 = readit.GetString(2);
                    r.Grad2 = readit.GetString(2);
                    r.Vpolaska = readit.GetString(3);
                    r.Vdolaska = readit.GetString(4);
                    r.Udaljenost = readit.GetString(5);
                    r.Prevoznik = readit.GetString(6);
                    lista.Add(r);
                }
                
                return lista;
            }
        }
        public int InsertRuta(Ruta r)
        {
            using (SqlConnection sqlConnection = new SqlConnection(cstring))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                int broj = this.GetRute().Count;
                sqlCommand.CommandText = "INSERT INTO Ruta VALUES(@Id,@Grad1,@Grad2,@Vpolaska,@Vdolaska,@Udaljenost,@Prevoznik)";
                sqlCommand.Parameters.AddWithValue("@Id", broj+3);
                sqlCommand.Parameters.AddWithValue("@Grad1", r.Grad1);
                sqlCommand.Parameters.AddWithValue("@Grad2", r.Grad2);
                sqlCommand.Parameters.AddWithValue("@Vpolaska", r.Vpolaska);
                sqlCommand.Parameters.AddWithValue("@Vdolaska", r.Vdolaska);
                sqlCommand.Parameters.AddWithValue("@Udaljenost", r.Udaljenost);
                sqlCommand.Parameters.AddWithValue("@Prevoznik", r.Prevoznik);


                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int UpdateRuta(Ruta r)
        {
            using (SqlConnection sqlConnection = new SqlConnection(cstring))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "UPDATE Ruta SET Grad1 = @Grad1, Grad2 = @Grad2, Vpolaska = @Vpolaska, Vdolaska = @Vdolaska, Udaljenost = @Udaljenost, Prevoznik = @Prevoznik WHERE Id = @Id";
                sqlCommand.Parameters.AddWithValue("@Id", r.Id);
                sqlCommand.Parameters.AddWithValue("@Grad1", r.Grad1);
                sqlCommand.Parameters.AddWithValue("@Grad2", r.Grad2);
                sqlCommand.Parameters.AddWithValue("@Vpolaska", r.Vpolaska);
                sqlCommand.Parameters.AddWithValue("@Vdolaska", r.Vdolaska);
                sqlCommand.Parameters.AddWithValue("@Udaljenost", r.Udaljenost);
                sqlCommand.Parameters.AddWithValue("@Prevoznik", r.Prevoznik);

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteRuta(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(cstring))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "DELETE FROM Ruta WHERE Id = " + id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

    }
}
